Jackson dependencies and helper utilities
This is a static helper utility class for quickly serialising and de-serialising JSON structures.
Uses a Jackson JsonFactory to perform the operations.
